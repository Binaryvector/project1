package project1.utils;


/**
 * 
 * This class allows to reuse old objects
 * @author Shinni
 *
 */
public class Pools {
	private static Map<Class, Pool> poolMap = new Map<Class, Pool>();
	
	public static <C> Pool<C> getPool(Class<C> type) {
		Pool<C> pool = poolMap.get(type);
		if (pool != null) {
			return pool;
		}
		pool = new Pool<C>(type);
		poolMap.add(type, pool);
		return pool;
	}
	
	public static <C> C get(Class<C> type) {
		return getPool(type).get();
	}
	
	public static void free(Object object) {
		getPool(object.getClass()).free(object);
	}
}
