package project1.utils;

public class Pool<C> {
	private Array<C> objects;
	private Class<C> type;
	
	public Pool(Class<C> type) {
		this.type = type;
	}
	
	public C get() {
		if (objects.isEmpty()) {
			try {
				return type.newInstance();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return objects.pop();
	}
	
	public void free(Object object) {
		objects.add((C) object);
	}
}
