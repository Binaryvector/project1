package project1.utils;

import java.util.Iterator;

public class Array<Type> implements Iterable<Type> {
	private Type[] data;
	private int capacity;
	private int size;
	private boolean ordered;
	private ArrayIterator<Type> iterator = new ArrayIterator<Type>();
	
	public Array() {
		this(true);
	}
	
	public Array(boolean ordered) {
		this(32, ordered);
	}
	
	public Array(int capacity, boolean ordered) {
		this.ordered = ordered;
		this.capacity = capacity;
		this.size = 0;
		data = (Type[])new Object[capacity];
	}
	
	public Array add(Type object) {
		checkCapacity(size + 1);
		data[size] = object;
		size += 1;
		return this;
	}
	
	public Type get(int index) {
		if (index > size || index < 0) {
			return null;
		}
		return data[index];
	}
	
	public int getIndex(Type object) {
		for (int i = 0; i < size; i++) {
			if (data[i] == object) {
				return i;
			}
		}
		return -1;
	}
	
	public void free() {
		Pool pool = Pools.getPool(this.first().getClass());
		while (!this.isEmpty()) {
			pool.free(this.pop());
		}
	}
	
	/**
	 * Removes the data at the given index. Runs in O(1) for unordered arrays, otherwise O(n)
	 * @param index
	 * @return this array for chaining
	 */
	public Array<Type> remove(int index) {
		size -= 1;
		if (ordered) {
			System.arraycopy(data, index + 1, data, index, size - index);
		} else {
			data[index] = data[size];
		}
		return this;
	}
	
	/**
	 * Removes the given object from the array. Runs always in O(n).
	 * @param object
	 * @return the position of the removed object. -1 when the object wasn't found.
	 */
	public int remove(Type object) {
		int index = getIndex(object);
		remove(index);
		return index;
	}
	
	public Type first() {
		return get(0);
	}
	
	public Type pop() {
		size -= 1;
		return data[size];
	}
	
	public boolean isEmpty() {
		return size == 0;
	}
	
	private void checkCapacity(int size) {
		if (size > capacity) {
			setCapacity(size * 3 / 2);
		}
	}
	
	public void setCapacity(int capacity) {
		this.capacity = capacity;
		Type[] newData = (Type[])new Object[capacity];
		System.arraycopy(data, 0, newData, 0, size);
		data = newData;
	}
	
	@Override
	public Iterator<Type> iterator() {
		return iterator.setup(data, size);
	}
	
	private static class ArrayIterator<Type> implements Iterator<Type> {
		private int index;
		private int size;
		private Type[] data;
		
		public ArrayIterator setup(Type[] data, int size) {
			this.data = data;
			this.size = size;
			this.index = 0;
			return this;
		}
		
		@Override
		public boolean hasNext() {
			return index < size;
		}
		
		@Override
		public Type next() {
			Type result = data[index];
			index += 1;
			return result;
		}
			
		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
}
