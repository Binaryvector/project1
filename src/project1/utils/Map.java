package project1.utils;

/**
 * @author Shinni
 *
 * @param <Key>
 * @param <Value>
 */
public class Map<Key, Value> {
	private Array<Key> keys;
	private Array<Value> values;
	
	public Map() {
		this(32);
	}
	
	public Map(int capacity) {
		keys = new Array(capacity, false);
		values = new Array(capacity, false);
	}
	
	public Value get(Key key) {
		int index = keys.getIndex(key);
		return values.get(index);
	}
	
	public Map add(Key key, Value value) {
		keys.add(key);
		values.add(value);
		return this;
	}
	
	public Map remove(Key key) {
		int index = keys.remove(key);
		values.remove(index);
		return this;
	}
}
