package project1.data;

public class User {
	private boolean admin;
	private String name;
	private String password;
	private String email;
	private int id;
	
	public User() {
		
	}
	
	public User setup(Object result) {
		//TODO setup object from DB result
		return this;
	}
	
	public boolean isAdmin() {
		return admin;
	}

	public String getName() {
		return name;
	}

	public void setName(String name, User user) {
		if (user != this && !user.isAdmin())
			return;
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password, User user) {
		if (user != this && !user.isAdmin())
			return;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email, User user) {
		if (user != this && !user.isAdmin())
			return;
		this.email = email;
	}

	public int getId() {
		return id;
	}
}
