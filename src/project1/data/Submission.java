package project1.data;

import project1.utils.Array;

public class Submission {
	private User owner;
	private Group group;
	private String name;
	private String description;
	private String timestamp;
	private int id;
	private Array<Document> documents;
}
